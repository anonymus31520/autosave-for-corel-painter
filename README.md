So some basics how to launch this since I am guessing that none of you are programmers. 

1. First you will have to **download python** if you don't have it. You can probably figure that part for your selves. I also recommend downloading additionally to the python the Visual Studio Code where you can really nicely see the code with colors... though that is optional.

2. Second you will **download** from my gitlab the **autosave.py file.** You can run it by opening Command prompt in windows and writing: python autosave.py.

**Be careful when you save the file put it into path (basically where the file is saved like desktop, download...) C:/
So like put it on disk C or whatever the main disc is called in your computer or else the command prompt will be unable to find it.**


There you have it. The python script will save the work Ctrl + S automatically every half an hour. If you want to change the interval at which it saves change the number in **INTERVAL = 1800**
(seconds)

Also when you launch it it will show black window that will write (print) a message every time it saves with the time and the window name on which it saved. (Don't close it or it will stop the script it is running you can minimalize it though)

Next for those who are feeling up to it you can set it up so that it launches simultaneously when you click icon on your desktop. 
(Like it opens both Corel Painter and the python script)

3. So **download** the second file called **launch_autosave.bat**. Put it on your desktop or wherever and **right click** it and **click edit** from the options.

4. There you will **edit the paths** which are the **C:/something...**
in my case. You will have to look for it on your computer where are the files stored and how exactly they are called so they know that to launch. When looking for Corel Painter the start will be probably the same but the year will probably differ in that stuff.

**Helpful tip when opening a folder in windows and searching for a path you can just copy the stuff like URL you have in browsers.**

When you manage to do all that you will then **save and exit**. Then you will **right click** on your **.bat file** and **click on create shortcut**.

Now when you** double click on the shortcut** it will launch both the Corel Painter and the python script that auto saves.

**You can rename it to Corel Painter and change the icon.**

You **change the icon** by r**ight clicking** then c**licking properties**. Then **Shortcut**, **Change icon** there the windows will let you choose some of theirs or you can download on from the internet - it has to .ico format for windows.

For those who want the **same icon as Corel Painter** you put real Corel Painter shortcut next to it follow the same steps for it and copy the path (look for icons in this file). Then do the same with out Shortcut and just paste the path from where you copied it there. That will give you the same options as the Real Corel Painter shortcut has.
- if you want any more information on this there are tutorials on the internet on how to change icons in the windows.

J**ust a reminder that every time you change the place (put it into a folder) the autosave.py you have to edit it (the path) in the .bat file pr it won't work.**

Also just as a precaution when creating the shortcut on the desktop from the .bat file do it when you know that will be the files permanent place like folder or somewhere if you don't want to have it on your desktop and then do that create a shortcut and just move that shortcut to the desktop.

That should be all. Hope this is helpfull.
