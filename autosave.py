import time
import pygetwindow as gw
import pyautogui

INTERVAL = 1800
PAINTER_WINDOW_NAME = "Corel Painter"

def detect_and_save():
    windows = gw.getAllTitles()

    filtered = filter(lambda x: PAINTER_WINDOW_NAME in x, windows)

    for name in filtered:
        print(f"saving window {name}")
        painter_window = gw.getWindowsWithTitle(name)[0]
        painter_window.activate()
        pyautogui.hotkey('ctrl', 's')
        print("Saved at", time.ctime())
    time.sleep(INTERVAL)

while True:
    detect_and_save()